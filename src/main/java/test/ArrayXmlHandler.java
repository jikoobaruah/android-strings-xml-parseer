package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ArrayXmlHandler extends DefaultHandler {

	private final String language;
	private final Callback callback;

	private boolean isTranslatable = true;
	private String key;
	private HashMap<String, List<String>> map;

	public ArrayXmlHandler(String language, Callback callback) {
		this.language = language;
		this.callback = callback;
		map = new HashMap<String, List<String>>();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equals("string-array")) {
			isTranslatable = true;
			for (int i = 0; i < attributes.getLength(); i++) {

				if ("translatable".equals(attributes.getQName(i)) && "false".equals(attributes.getValue(i))) {
					isTranslatable = false;
				}

				if ("name".equals(attributes.getQName(i)))
					key = attributes.getValue(i);
			}
		} else if (qName.equals("item")) {

		}
	}

	private static String oldChars = "";

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (!isTranslatable)
			return;
		if (key == null)
			return;

		String temp = new String(ch, start, length).trim();
		if (temp == null || temp.length() == 0) {
			List<String> list = map.get(key);
			if (list == null)
				list = new ArrayList<String>();
			if (oldChars != null && oldChars.length() > 0) {
				list.add(oldChars.trim());
				map.put(key, list);
				oldChars = "";
			}
		} else {
			oldChars = oldChars + temp;
		}

	}

	@Override
	public void endDocument() throws SAXException {
		if (callback != null)
			callback._onLanguageDone(language, map);
	}

	public interface Callback {
		void _onLanguageDone(String language, Map<String, List<String>> result);
	}

}
