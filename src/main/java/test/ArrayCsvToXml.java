package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class ArrayCsvToXml {

	private static final String SAMPLE_CSV_FILE_PATH = "input/csv/arrays.csv";
	
	public static void main(String[] args) throws IOException {
		Map<String, Map<String,List<String>> > map = new HashMap<String, Map<String,List<String>>>();
		Map<Integer, String> columnLanguageMap = new HashMap<Integer, String>();
		
		try (Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {
			int row = 0;
			for (CSVRecord csvRecord : csvParser) {

				for (int i = 1; i < csvRecord.size(); i++) {

					if (row == 0) {
						System.out.println(csvRecord.get(i));
						map.put(csvRecord.get(i), null);
						columnLanguageMap.put(i, csvRecord.get(i));
					}  else {
						Map<String, List<String>> arrayMap = map.get(columnLanguageMap.get(i));
						if (arrayMap == null)
							arrayMap = new HashMap<String, List<String>>();
						String arrayId = csvRecord.get(0);
						List<String> array = arrayMap.get(arrayId);
						if (array == null) array = new ArrayList<String>();
						array.add(csvRecord.get(i));
						arrayMap.put(arrayId, array);
						map.put(columnLanguageMap.get(i), arrayMap);
					}
				}

				row++;
			}
		}
		
		for (Entry<String, Map<String,List<String>>> entry : map.entrySet()) {
			
			writeArrayXml(entry.getKey(), entry.getValue());
			
			
			for (Entry<String,List<String>> e  : entry.getValue().entrySet()) {
				System.err.println("id > "+ e.getKey());
				System.err.println("list > "+ e.getValue());
				
				System.err.println("===============================");
				
			}
			
			System.out.println("++++++++++++++++++++++++++++++++++");
			
		}
		
	}

	private static final String array_template_open = "<string-array name=\"%s\">";
	private static final String array_template_close = "</string-array>";
	private static final String array_item_template = "<item>%s</item>";
	
	private static void writeArrayXml(String key, Map<String, List<String>> value) throws IOException {
		
		File file = new File("output");
		if (!file.exists())
			file.mkdir();
		file = new File("output/xmls");
		if (!file.exists())
			file.mkdir();

		file = new File("output/xmls/"+key);
		if (!file.exists())
			file.mkdir();

		
		file = new File(file.getAbsolutePath() + "/arrays.xml");
		if (file.exists())
			file.delete();

		FileWriter fileWriter = null;
		file.createNewFile();

		StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		sb.append("\n");
		sb.append("<resources>");
		
		for (Entry<String,List<String>> e  : value.entrySet()) {
			sb.append("\n");
			sb.append("\t").append(String.format(array_template_open, e.getKey()));
			
			for (String s : e.getValue()) {
				sb.append("\n");
				sb.append("\t").append("\t").append(String.format(array_item_template, s));
			}
			sb.append("\n");
			sb.append("\t").append(array_template_close);
			 
		}
		sb.append("\n");
		sb.append("</resources>");

		fileWriter = new FileWriter(file);
		fileWriter.write(sb.toString());
		fileWriter.flush();
		fileWriter.close();
		
	}

//	private static void writeLanguageXmls(Map<String, Map<String, String>> languageMap) throws IOException {
//		for (Entry<String, Map<String, String>> entry : languageMap.entrySet()) {
//			writeXml(entry.getKey(), entry.getValue());
//		}
//
//	}
//
//	private static final String template = "<string name=\"%s\">%s</string>";
//
//	private static void writeXml(String key, Map<String, String> value) throws IOException {
//		File file = new File("output");
//		if (!file.exists())
//			file.mkdir();
//		file = new File("output/xmls");
//		if (!file.exists())
//			file.mkdir();
//
//		file = new File("output/xmls/"+key);
//		if (!file.exists())
//			file.mkdir();
//
//		
//		file = new File(file.getAbsolutePath() + "/strings.xml");
//		if (file.exists())
//			file.delete();
//
//		FileWriter fileWriter = null;
//		file.createNewFile();
//
//		StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
//		sb.append("\n");
//		sb.append("<resources>");
//		for (Entry<String, String> map : value.entrySet()) {
//			if (map.getValue() != null && map.getValue().trim().length() > 0) {
////				sb.append("\n");
////				sb.append("\t").append(String.format(template, map.getKey(),  StringEscapeUtils.escapeXml(map.getValue().trim()) ));
//				if (!map.getKey().trim().isEmpty() && !map.getKey().startsWith("ഇ")) {
//					sb.append("\n");
//					sb.append("\t").append(String.format(template, map.getKey().trim(),  map.getValue().trim().replace("&", "&amp;")));
//				}	   
//			}
//		}
//		sb.append("\n");
//		sb.append("</resources>");
//
//		fileWriter = new FileWriter(file);
//		fileWriter.write(sb.toString());
//		fileWriter.flush();
//		fileWriter.close();
//
//	}

}
