package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.opencsv.CSVWriter;

import test.XmlHandler.Callback;

public class XmlToCsv {

	public static final String[] lnCodes = { "", "bn", "gu", "hi", "kn", "ml", "mr", "ta", "te" };

	public static Map<String, Set<String>> missingLanguageKeys;

	public static Map<String, Map<String, String>> languageMap;

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		missingLanguageKeys = new HashMap<String, Set<String>>();
		languageMap = new HashMap<String, Map<String, String>>();

		for (String language : lnCodes) {
			getMapForLanguage(language, saxParser);
		}
	}

	private static void getMapForLanguage(String language, SAXParser saxParser) throws SAXException, IOException {
		String fileName = "input/xmls/values%s/strings.xml";
		String placeholder = "";
		if (language != null && !language.equals(""))
			placeholder = "-" + language;
		String finalFileName = String.format(fileName, placeholder);

		saxParser.parse(new File(finalFileName), new XmlHandler(language, new Callback() {
			public void onLanguageDone(String language, Map<String, String> result) {
				languageMap.put(language, result);
				if (languageMap.size() == lnCodes.length) {
					performActions(languageMap);
				}

			}
		}));
	}

	private static void performActions(Map<String, Map<String, String>> languageMap) {
		for (String language : lnCodes) {
			findMissingTranslations(languageMap, language);
		}

//		for (String key : missingLanguageKeys.keySet()) {
//			StringBuilder sb = new StringBuilder();
//			for (String s : missingLanguageKeys.get(key)) {
//				sb.append(s);
//				sb.append(" , ");
//			}
//		}

		try {
			writeMissingToCsv();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			writeAllToCsv();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static void findMissingTranslations(Map<String, Map<String, String>> languageMap, String language) {
		if (language == "")
			return;

		Set<String> defaultStringKeys = languageMap.get("").keySet();

		Set<String> languageStringKeys = languageMap.get(language).keySet();

		List<String> missingKeys = new ArrayList<String>();
		for (String s : defaultStringKeys) {
			if (!languageStringKeys.contains(s)) {
				missingKeys.add(s);

				if (missingLanguageKeys.containsKey(s)) {
					missingLanguageKeys.get(s).add(language);

				} else {
					missingLanguageKeys.put(s, new HashSet<String>(Arrays.asList(language)));
				}

			}
		}

	}


	private static void writeAllToCsv() throws IOException {
		File file = new File("output");
		if (file.exists()) {
			file.delete();
		}
		file.mkdir();

		String csvFile = "output/consolidated.csv";

		FileWriter outputfile = new FileWriter(csvFile);

		CSVWriter writer = new CSVWriter(outputfile);

		Set<String> masterKeySet = languageMap.get("").keySet();

		String[] header = { "key", "default", "bengali", "gujrati", "hindi", "kannada", "malayalam", "marathi", "tamil","telegu" };
		writer.writeNext(header);

		List<String[]> data = new ArrayList<String[]>();

		for (String key : masterKeySet) {

			String[] array = new String[header.length];
			array[0] = key;
			int c = 1;
			for (String s : lnCodes) {
				array[c] = languageMap.get(s).get(key);
				c++;
			}

			data.add(array);
		}

		writer.writeAll(data);

		writer.close();
		
	}

	private static void writeMissingToCsv() throws IOException {

		File file = new File("output");
		if (file.exists()) {
			file.delete();
		}
//		dialog_event_game

		file.mkdir();

		String csvFile = "output/missing.csv";

		FileWriter outputfile = new FileWriter(csvFile);

		CSVWriter writer = new CSVWriter(outputfile);

		// adding header to csv
		String[] header = { "key", "default", "bengali", "gujrati", "hindi", "kannada", "malayalam", "marathi", "tamil",
				"telegu" };
		writer.writeNext(header);

		List<String[]> data = new ArrayList<String[]>();

		for (String key : missingLanguageKeys.keySet()) {

			String[] array = new String[header.length];
			array[0] = key;
			int c = 1;
			for (String s : lnCodes) {
				array[c] = languageMap.get(s).get(key);
				c++;
			}

			data.add(array);
		}

		writer.writeAll(data);

		writer.close();

	}


}
