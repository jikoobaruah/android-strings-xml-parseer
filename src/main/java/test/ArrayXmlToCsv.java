package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.opencsv.CSVWriter;

import test.ArrayXmlHandler.Callback;

public class ArrayXmlToCsv {

	public static final String[] lnCodes = { "", "bn", "gu", "hi", "kn", "ml", "mr", "ta", "te" };

	public static Map<String, Map<String, List<String>>> languageMap;

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		languageMap = new HashMap<String, Map<String, List<String>>>();

		for (String language : lnCodes) {
			getMapForLanguage(language, saxParser);
		}

	}

	private static void getMapForLanguage(String language, SAXParser saxParser) throws SAXException, IOException {
		String fileName = "input/xmls/values%s/arrays.xml";
		String placeholder = "";
		if (language != null && !language.equals(""))
			placeholder = "-" + language;
		String finalFileName = String.format(fileName, placeholder);

		saxParser.parse(new File(finalFileName), new ArrayXmlHandler(language, new Callback() {

			@Override
			public void _onLanguageDone(String language, Map<String, List<String>> result) {
				languageMap.put(language, result);
				if (languageMap.size() == lnCodes.length) {
					performActions(languageMap);
				}

			}

		}));

	}

	private static void performActions(Map<String, Map<String, List<String>>> languageMap) {

		List<List<String>> rows = new ArrayList<List<String>>();
		Set<String> keySet = languageMap.get("").keySet();

		for (String key : keySet) {

			List<String> list = languageMap.get("").get(key);
			int size = list.size();

			for (int i = 0; i < size; i++) {
//				StringBuilder sb = new StringBuilder(key).append(",");
				ArrayList<String> stringList = new ArrayList<String>();
				stringList.add(key);
				for (String langCode : lnCodes) {
					String s = languageMap.get(langCode).get(key).get(i);
//					sb.append(s).append(",");
					stringList.add(s);
				}
//				System.out.println(sb);
				rows.add(stringList);

			}
		}
		
		
//		for (List<String> row : rows) {
//			System.out.println(row);
//		}

		try {
			writeAllToCsv(rows);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private static void writeAllToCsv(List<List<String>> rows) throws IOException {
		File file = new File("output");
		if (file.exists()) {
			file.delete();
		}
		file.mkdir();

		String csvFile = "output/arrays.csv";

		FileWriter outputfile = new FileWriter(csvFile);

		CSVWriter writer = new CSVWriter(outputfile);

		Set<String> masterKeySet = languageMap.get("").keySet();

		String[] header = { "key", "default", "bengali", "gujrati", "hindi", "kannada", "malayalam", "marathi", "tamil","telegu" };
		writer.writeNext(header);
//
		List<String[]> data = new ArrayList<String[]>();
//
//		for (String key : masterKeySet) {
//
//			String[] array = new String[header.length];
//			array[0] = key;
//			int c = 1;
//			for (String s : lnCodes) {
//				array[c] = languageMap.get(s).get(key);
//				c++;
//			}
//
//			data.add(array);
//		}
		
		for (List<String> row : rows) {
			data.add(row.toArray(new String[row.size()]));
		}

		writer.writeAll(data);

		writer.close();
		
	}

}
