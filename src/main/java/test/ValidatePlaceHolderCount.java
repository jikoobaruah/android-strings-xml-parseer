package test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import test.XmlHandler.Callback;

/**
 * Validate if strings in xmls of different languages have the same %s count
 * @author dell
 *
 */
public class ValidatePlaceHolderCount {

	public static final String[] lnCodes = { "", "bn", "gu", "hi", "kn", "ml", "mr", "ta", "te" };

	public static Map<String, Set<String>> missingLanguageKeys;

	public static Map<String, Map<String, String>> languageMap;

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		missingLanguageKeys = new HashMap<String, Set<String>>();
		languageMap = new HashMap<String, Map<String, String>>();

		for (String language : lnCodes) {
			getMapForLanguage(language, saxParser);
		}
	}

	private static void getMapForLanguage(String language, SAXParser saxParser) throws SAXException, IOException {
		String fileName = "input/xmls/values%s/strings.xml";
		String placeholder = "";
		if (language != null && !language.equals(""))
			placeholder = "-" + language;
		String finalFileName = String.format(fileName, placeholder);

		saxParser.parse(new File(finalFileName), new XmlHandler(language, new Callback() {
			public void onLanguageDone(String language, Map<String, String> result) {
				languageMap.put(language, result);
				if (languageMap.size() == lnCodes.length) {
					performActions();
				}

			}
		}));
	}

	private static void performActions() {
		Map<String, String> defaultMap = languageMap.get("");
		for (String s : defaultMap.keySet()) {
			validatePlaceHolderCount(s);
		}
//		validatePlaceHolderCount("game_start_title");
	}

	private static void validatePlaceHolderCount(String key) {
		int[] counts = new int[lnCodes.length];

		for (int i = 0; i < counts.length; i++) {
			counts[i] = getPlaceholderCount(key, lnCodes[i]);
		}

		boolean isMismatch = false;
		StringBuilder sb = new StringBuilder();
		sb.append(counts[0]);
		for (int i = 1; i < counts.length; i++) {
			if (counts[0] != -1 && counts[i] != -1 && counts[0] != counts[i])
				isMismatch = true;
			sb.append(",").append(counts[i]);
		}

		if (isMismatch) {
			System.out.println(key + ">> " + sb);
		}

	}

	private static int getPlaceholderCount(String key, String language) {
//		System.out.println(key + "  "+ language);
		int count = 0;
		String input = languageMap.get(language).get(key);
		if (input == null)
			return -1;
		int index = input.indexOf("%s");
		while (index >= 0) {
			count++;
			index = input.indexOf("%s", index + 1);
		}
		return count;

	}

}
