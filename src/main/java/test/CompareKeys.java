package test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import test.XmlHandler.Callback;

public class CompareKeys {
	
	static Map<String, String> map1 = new HashMap<String, String>();
	static Map<String, String> map2 = new HashMap<String, String>();
	static private boolean isTwoDone;
	static private boolean isOneDone;
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();
		
		
		String file1 = "input/compare/telegu.xml";
		String file2 = "input/compare/strings.xml";
		
		
		saxParser.parse(new File(file1), new XmlHandler("", new Callback() {
			public void onLanguageDone(String language, Map<String, String> result) {
				 map1 = result;
				 isOneDone = true;
				 compareMaps();
			}
		}));
		
		saxParser.parse(new File(file2), new XmlHandler("", new Callback() {
			

			public void onLanguageDone(String language, Map<String, String> result) {
				 map2 = result;
				 isTwoDone = true;
				 compareMaps();
			}
		}));
		
	}

	protected static void compareMaps() {
		if (!isOneDone || !isTwoDone)
			return;
		
		System.out.println(map1.size());
		System.out.println(map2.size());
		
		for (String string : map2.keySet()) {
			if (!map1.containsKey(string))
				System.err.println(string);
		}
		
		
		
	}
	
	
 

}
