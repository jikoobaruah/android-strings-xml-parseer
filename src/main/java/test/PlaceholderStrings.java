package test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import test.XmlHandler.Callback;

/**
 * find out strings with multiple placeholders
 * @author dell
 *
 */
		
public class PlaceholderStrings {



	public static Map<String, Map<String, String>> languageMap;

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		languageMap = new HashMap<String, Map<String, String>>();

		getMapForLanguage(saxParser);
	}

	private static void getMapForLanguage(SAXParser saxParser) throws SAXException, IOException {
		String finalFileName = "input/xmls/values/strings.xml";
	 
		saxParser.parse(new File(finalFileName), new XmlHandler("en", new Callback() {
			public void onLanguageDone(String language, Map<String, String> result) {
				languageMap.put(language, result);
				performActions(languageMap);
			 
			}
		}));
	}

	protected static void performActions(Map<String, Map<String, String>> languageMap2) {
		Map<String,String> stringMap = languageMap2.get("en");
		int i =0;
		for (Entry<String,String> entry : stringMap.entrySet() ) {
			if (entry.getValue().contains("%s")  ) {
				i ++;
				System.out.println( i + " :::  " + entry.getKey() + " >> " +  entry.getValue() );
			}
		}
		
	}

}
