package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

public class CsvToXml2 {

	private static final String SAMPLE_CSV_FILE_PATH = "input/csv/final_strings.csv";

//	private static final String[] unused = new String[] {"chat_notif_title_suffix",
//			"friends_group_male",
//			"baby_says_xtips",
//			"baby_date_due",
//			"text_days_parent",
//			"home_chat_info",
//			"save_text",
//			"question_posted_success",
//			"doctor_says",
//			"menu_notify_off",
//			"please_choose_relevant_date_my_week",
//			"chat_user_joined",
//			"my_fruit_size",
//			"baby_date_week",
//			"chat_event_title",
//			"formumma",
//			"feed_date_value",
//			"general_advice",
//			"notif_mute",
//			"result_right_title",
//			"fordaddy",
//			"live_session_info",
//			"healofy_says",
//			"for_mom",
//			"you_got_life",
//			"like_done",
//			"baby_says",
//			"feed_nutrition_next",
//			"wifey",
//			"live_comment_disable",
//			"chat_friends_text",
//			"chat_event_message_mom",
//			"notification_disabled",
//			"pfp_chat_info",
//			"get_your_doubts_cleared_by_a_doctor",
//			"x_days",
//			"conceive_in_days",
//			"result_next_info",
//			"live_session_ended",
//			"share_with_x",
//			"dads",
//			"for_baby",
//			"baby_in_days",
//			"hubby",
//			"chat_notify_text",
//			"buy_earn_text",
//			"feed_nutrition_title",
//			"date_type_birth",
//			"write_your_question_here",
//			"drop_off_title",
//			"live_session_with_doctor_is_starting_soon",
//			"dad_i_am",
//			"conceive_text",
//			"result_next_life",
//			"result_wrong_title",
//			"connect_in_language",
//			"healofy_says_xtips",
//			"user_gender_changed",
//			"mom_i_am",
//			"ask",
//			"feed_nutrition_current",
//			"mom_daily",
//			"share_done",
//			"baby_days_text",
//			"drop_off_message",
//			"do_you_know_x",
//			"feed_info_meta",
//			"weekly_tip_title",
//			"msg_max_comments",
//			"help_her_with_your_answer",
//			"like_text",
//			"chat_event_message_dad",
//			"user_un_blocked",
//			"moms",
//			"general_advice_xtips",
//			"planning_group_icon",
//			"your_chat_group_has_been_changed",
//			"menu_notify_on",
//			"result_wrong_detail",
//			"text_days_pregnant",
//			"chat_user_anonymous",
//			"baby_date_lmp",
//			"unread_messages_count",
//			"feed_nutrition_info",
//			"qna_question_posted",
//			"friends_group_female",
//			"date",
//			"question_asked_secretly",
//			"result_right_detail",
//			"your_daily",
//			"monthly_chat_group",
//			"language_group",
//			"dad_daily",
//			"week",
//			"baby_date_birth",
//			"doctor_says_xtips",
//			"date_pfp",
//			"date_type_due",
//			"buy_earn_title",
//			"friends_group_info",
//			"daily_tip_message2",
//			"daily_tip_message1",
//			"save_done",
//			"dialog_language_info",
//			"dialog_language_title_mom",
//			"dialog_language_title_dad",
//			"thread_follow_okay"};

	private static  HashSet<String> unUsedSet;// = new HashSet<String>();//Arrays.asList(unused));
	
	static {
		

		FileReader propReader;
		try {
			propReader = new FileReader("src/main/resources/unUsed.properties");
			Properties p = new Properties();
			p.load(propReader);
			unUsedSet = new HashSet<String>(Arrays.asList(p.getProperty("strings").replace(" ", "").split(",")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	

	public static void main(String[] args) throws IOException {

		System.out.println(unUsedSet.size());
		Map<String, Map<String, String>> languageMap = new HashMap<String, Map<String, String>>();
		Map<Integer, String> columnLanguageMap = new HashMap<Integer, String>();

		try (Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);) {
			int row = 0;
			for (CSVRecord csvRecord : csvParser) {

				for (int i = 1; i < csvRecord.size(); i++) {

					if (row == 0) {
						languageMap.put(csvRecord.get(i), new LinkedHashMap<String, String>());
						columnLanguageMap.put(i, csvRecord.get(i));
					} else {
						languageMap.get(columnLanguageMap.get(i)).put(csvRecord.get(0), csvRecord.get(i));
					}
				}

				row++;
			}
			writeLanguageXmls(languageMap);
		}
	}

	private static void writeLanguageXmls(Map<String, Map<String, String>> languageMap) throws IOException {
		for (Entry<String, Map<String, String>> entry : languageMap.entrySet()) {
			writeXml(entry.getKey(), entry.getValue());
		}

	}

	private static final String template = "<string name=\"%s\">%s</string>";

	private static void writeXml(String key, Map<String, String> value) throws IOException {

		File file = new File("output");
		if (!file.exists())
			file.mkdir();
		file = new File("output/xmls");
		if (!file.exists())
			file.mkdir();

		file = new File("output/xmls/" + key);
		if (!file.exists())
			file.mkdir();

		file = new File(file.getAbsolutePath() + "/strings.xml");
		if (file.exists())
			file.delete();

		FileWriter fileWriter = null;
		file.createNewFile();

		StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
		sb.append("\n");
		sb.append("<resources>");
		for (Entry<String, String> map : value.entrySet()) {
			if (map.getValue() != null && map.getValue().trim().length() > 0) {
//				sb.append("\n");
//				sb.append("\t").append(String.format(template, map.getKey(),  StringEscapeUtils.escapeXml(map.getValue().trim()) ));
				if (!map.getKey().trim().isEmpty() && !map.getKey().startsWith("ഇ")
						&& !unUsedSet.contains(map.getKey().trim())) {
					sb.append("\n");
					sb.append("\t").append(
							String.format(template, map.getKey().trim(), map.getValue().trim().replace("&", "&amp;")));
				}
			}
		}
		sb.append("\n");
		sb.append("</resources>");

		fileWriter = new FileWriter(file);
		fileWriter.write(sb.toString());
		fileWriter.flush();
		fileWriter.close();

	}

}
