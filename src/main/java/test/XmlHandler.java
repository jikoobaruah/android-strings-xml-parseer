package test;

import java.util.HashMap;
import java.util.Map;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XmlHandler extends DefaultHandler{
	
	private final String language;
	private final Callback callback;

	private boolean isTranslatable = true;
	private String key;
	private HashMap<String, String> map;


	public XmlHandler(String language, Callback callback) {
		this.language = language;
		this.callback = callback;
		map = new HashMap<String, String>();
	}
	
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes)
			throws SAXException {
		

		key = null;
		if (qName.equals("string")) {
			isTranslatable = true;
			for (int i = 0; i < attributes.getLength(); i++) {
				if ("translatable".equals(attributes.getQName(i)) && "false".equals(attributes.getValue(i))) {
					isTranslatable = false;
				}
				
				if ("name".equals(attributes.getQName(i)))
					key = attributes.getValue(i);
			}
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		if (!isTranslatable)
			return;
		if (key == null)
			return;
		
		String oldValue  = map.get(key);
		if (oldValue == null) oldValue = "";
		
		String newString = new String(ch,start,length);
		if (newString.contains("</"))
			newString = "<![CDATA[" + newString + "]]>";
		
		map.put(key, oldValue+ newString);
	}
	
	@Override
	public void endDocument() throws SAXException {
		if (callback != null)
			callback.onLanguageDone(language,map);
	}

	
	
	
	public interface Callback{
		void onLanguageDone(String language, Map<String, String> result );
	}

}
