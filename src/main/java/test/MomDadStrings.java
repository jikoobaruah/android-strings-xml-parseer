package test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.opencsv.CSVWriter;

import test.XmlHandler.Callback;

public class MomDadStrings {
	
	public static Map<String, Map<String, String>> languageMap;
	public static final String[] lnCodes = { "", "bn", "gu", "hi", "kn", "ml", "mr", "ta", "te" };
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		languageMap = new HashMap<String, Map<String, String>>();

		for (String language : lnCodes) {
			getMapForLanguage(language, saxParser);
		}
	}

	private static void getMapForLanguage(String language, SAXParser saxParser) throws SAXException, IOException {
		String fileName = "input/mom-dad/values%s/strings.xml";
		String placeholder = "";
		if (language != null && !language.equals(""))
			placeholder = "-" + language;
		String finalFileName = String.format(fileName, placeholder);

		saxParser.parse(new File(finalFileName), new XmlHandler(language, new Callback() {
			public void onLanguageDone(String language, Map<String, String> result) {
				languageMap.put(language, result);
				if (languageMap.size() == lnCodes.length) {
					performActions();
				}
			}
		}));
	}

	protected static void performActions() {
		Map<String,String> stringMap = languageMap.get("");
		Map<String,String> genderMap = new HashMap<String, String>();
		

		
		for (Entry<String,String> entry : stringMap.entrySet() ) {
			if (entry.getValue().contains("mom") || entry.getKey().contains("mom") || entry.getValue().contains("Mumma") ||  entry.getValue().contains("dad") || entry.getKey().contains("dad") || entry.getValue().contains("Daddy")  ) {
				genderMap.put(entry.getKey(), entry.getValue());
			}
			
			 
		}
		
		System.out.println("gender strings >> ");
		for (Entry<String,String> entry : genderMap.entrySet() ) {
			System.out.println(entry.getKey() );
		}
		
		System.out.println(genderMap.size());
		
		try {
			writeAllGenderStringsToCsv(genderMap);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
	}
	
	
	private static void writeAllGenderStringsToCsv(Map<String, String> genderMap) throws IOException {
		File file = new File("output");
		if (file.exists()) {
			file.delete();
		}
		file.mkdir();

		String csvFile = "output/mom-dad.csv";

		FileWriter outputfile = new FileWriter(csvFile);

		CSVWriter writer = new CSVWriter(outputfile);

		Set<String> masterKeySet = genderMap.keySet();

		String[] header = { "key", "default", "bengali", "gujrati", "hindi", "kannada", "malayalam", "marathi", "tamil","telegu" };
		writer.writeNext(header);

		List<String[]> data = new ArrayList<String[]>();

		for (String key : masterKeySet) {

			String[] array = new String[header.length];
			array[0] = key;
			int c = 1;
			for (String s : lnCodes) {
				array[c] = languageMap.get(s).get(key);
				c++;
			}

			data.add(array);
		}

		writer.writeAll(data);

		writer.close();
		
	}
	
}
